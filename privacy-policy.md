# Privacy Policy

Solid New Tab requires authentication to Google Drive to use it for synchronization of the content between Chrome instances.
For this purpose it creates a config file on the drive which it reads and writes to as the extension settings are changed.
Data between extension and Google Drive are exchanged through Google REST API and require user's agreement.

- Extension doesn't read or modify any other files on the Google Drive.
- Extension doesn't use or store your profile information.
- Extension doesn't share or sell the data no furhter parties.

In case synchronization is not wished to be used anymore, data can be manually deleted from the Google Drive.

Contact: matus.benko@gmail.com

# Solid New Tab

![Screenshot](metadata/screenshot-default.png)

Solid New Tab is extension for Chrome browser which provides custom new tab page. 
Page shows links organized into the columns. 
Each link has an icon which is manually chosen from the set of free fontawesome icons. 
Links are fully customizable through simple settings editor on the options page.

It is possible to use preddefined home and work environments with different set of links. 
The plan for the future is to allow configuration of the environemnts.

For the power users the extension also provides keyboard support which is compatible with vimium extension.

## Installation

Install from [Chrome Web Store](https://chrome.google.com/webstore/detail/solid-new-tab/fillnkhhkimbilnofnpjlnfkahbkabld).
For the local development [load unpacked extension](https://developer.chrome.com/extensions/getstarted).

## Usage

For the configuration and list of supported shortcuts open options page.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)

<a href="https://www.buymeacoffee.com/0lqAi6k" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="51" width="217" ></a>
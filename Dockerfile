FROM openjdk:13-alpine

# Install Maven
RUN apk add --no-cache curl tar bash
ARG MAVEN_VERSION=3.3.9
ARG USER_HOME_DIR="/root"
RUN mkdir -p /usr/share/maven && \
    curl -fsSL http://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | tar -xzC /usr/share/maven --strip-components=1 && \
    ln -s /usr/share/maven/bin/mvn /usr/bin/mvn
ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

# Install Chrome Web Driver
RUN apk add --no-cache chromium chromium-chromedriver xvfb
ENV DISPLAY :99
RUN echo 'echo "Starting Xvfb"' >> /xvfb-start.sh && \
    echo "`which Xvfb` $DISPLAY -dpi 100 -screen 0 1920x1080x24 > /dev/null 2>&1 &" >> /xvfb-start.sh && \
    echo 'echo "Done"' >> /xvfb-start.sh

# Add project
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY pom.xml /usr/src/app
RUN mvn -T 1C install -DskipAssembly=true && rm -rf target
COPY src /usr/src/app/src

# Create screenshot folder
RUN mkdir -p /usr/screenshots

CMD ["/bin/sh", "-c", "sh /xvfb-start.sh && mvn package -Ddocker=true"]


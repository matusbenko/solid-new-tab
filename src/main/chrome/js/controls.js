import * as settings from "./settings.js";

const notificationVersion = 10100;

export function initialize() {
    addSettingsHandler();

    settings.getLastOpenNotification((openedVersion) => {
        if (openedVersion < notificationVersion) {
            $("#notification i").addClass("bounce");
        }
    });

    $('#notification').click(() => {
        settings.setLastOpenNotification(notificationVersion);
        $('#notification-window').modal('show');
        $('#notification i').removeClass("bounce");
    });
}

function addSettingsHandler() {
    $('#settings').click(() => chrome.runtime.openOptionsPage());
}

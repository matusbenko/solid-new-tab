export const defaultAppearanceSettings = {
    background: {
        imageUrl: undefined,
        opacity: 0.8
    },
    fontSize: {
        sectionTitle: 3,
        link: 0.875
    },
    columns: {
        colSpan: 2,
        padding: {
            left: 15,
            top: 100,
            right: 15,
            bottom: 0
        },
        verticalAlignment: 'align-items-start',
        horizontalAlignment: 'justify-content-center'
    }
};

export const defaultSettings = {
    version: '1.1',
    lastOpenNotification: 10100,
    keyboardSupport: false,
    environment: [
        {
            id: '0bb2d437-8509-439e-b410-190421b75563',
            name: 'Home',
            active: false,
            icon: 'fas fa-home',
            content:
                `=Tools
fas fa-envelope, Gmail, https://mail.google.com/mail/u/0/#inbox
fas fa-map, Maps, https://www.google.com/maps
fab fa-google-drive, Drive, https://drive.google.com/drive/u/0/my-drive
-
fas fa-language, Translate, https://translate.google.com/
fas fa-spell-check, Dictionary, https://www.dictionary.com/
-
fab fa-paypal, Paypal, https://www.paypal.com/signin?country.x=DE&locale.x=de_DE

=Gaming
fab fa-steam, ProtonDB, https://www.protondb.com/explore
fas fa-gamepad, IGN, https://www.ign.com/reviews/games
fas fa-heart, Indie Games Plus, https://indiegamesplus.com/

=Social
fab fa-twitter, Twitter, https://twitter,com
fab fa-facebook, Facebook, https://facebook,com
fab fa-mastodon, Mastodon, https://mastodon.social
-
fab fa-youtube, YouTube, http://youtube.com
fab fa-reddit, Reddit, https://reddit.com
fab fa-mixcloud, Mixcloud, https://www.mixcloud.com/
fab fa-goodreads-g, Goodreads, https://www.goodreads.com/review/list/50681851

=Programming
fab fa-gitlab, GitLab, https://gitlab.com/explore/projects/trending
fab fa-github, GitHub, https://github.com/explore
fab fa-bitbucket, BitBucket, https://bitbucket.org

=News
fas fa-newspaper, DW, https://www.dw.com/en/top-stories/s-9097
fas fa-newspaper, RT, https://www.rt.com/
fas fa-newspaper, France24, https://www.france24.com/en/
fas fa-newspaper, BBC, https://www.bbc.com/
fas fa-newspaper, CNN, https://edition.cnn.com/
fas fa-newspaper, CGTN, https://www.cgtn.com/
-
fas fa-rss, Medium, https://medium.com/
fas fa-rss, It's FOSS, https://itsfoss.com/
fas fa-rss, opensource.com, https://opensource.com/`,
            theme: 'default',
            appearance: defaultAppearanceSettings
        },
        {
            id: '42e462b0-c684-4bc4-9d0e-7f2327f4ed7b',
            name: 'Work',
            active: true,
            icon: 'fas fa-briefcase',
            content:
                `=Knowledge
fas fa-book, O'Reilly Safari books, https://learning.oreilly.com/
fas fa-seedling, Spring projects, https://spring.io/projects/
fab fa-chrome, Chrome extension tutorial, https://developer.chrome.com/extensions/getstarted

=Docs
fas fa-book, JDK 13, https://docs.oracle.com/en/java/javase/13/docs/api/index.html
fas fa-book, Spring framework API, https://docs.spring.io/spring/docs/current/javadoc-api/
fas fa-book, NodeJS API, https://nodejs.org/dist/latest-v8.x/docs/api/
-
fas fa-book, Angular API, https://angular.io/api
fas fa-book, React Top-Level API, https://reactjs.org/docs/react-api.html
-
fas fa-book, Vue.js API, https://vuejs.org/v2/api/
fas fa-book, AngularJS API, https://docs.angularjs.org/api
fas fa-book, jQuery API, https://api.jquery.com/

=Tools
fas fa-hammer, Gradle, https://gradle.org/
fas fa-hammer, Maven, https://maven.apache.org/
-
fas fa-book, Solr, https://lucene.apache.org/solr/
fas fa-book, Elastic Search, https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-elastic-stack.html
-
fas fa-book, Apache commons, http://commons.apache.org
=Databases
fas fa-book, MySQL Reference, https://dev.mysql.com/doc/refman/8.0/en/
fas fa-book, MongoDB Manual, https://docs.mongodb.com/manual/
fas fa-book, Oracle DB SQL reference (quick), https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlqr/index.html
fas fa-book, Oracle DB SQL reference, https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/index.html`,
            theme: 'default',
            appearance: defaultAppearanceSettings
        }
    ]
};

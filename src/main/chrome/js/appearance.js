import * as settings from "./settings.js";

function setEnvironment(environmentId, callback) {
    settings.getAppearance(environmentId, function (appearance) {
        if (appearance.background.imageUrl !== undefined) {
            $('#background-underlay')
                .css('background-image', `url(${appearance.background.imageUrl})`)
                .css('background-size', '100% 100%')
                .css('opacity', appearance.background.opacity);
        } else {
            $('#background-underlay')
                .css('background', 'none')
                .css('opacity', 1);
        }
        $('body').css('font-size', appearance.fontSize.link + 'rem');
        $('#row1')
            .removeClass()
            .addClass("row")
            .addClass(appearance.columns.verticalAlignment)
            .addClass(appearance.columns.horizontalAlignment);

        callback();
    });
}

export {setEnvironment};
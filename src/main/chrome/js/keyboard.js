// ----------------------------------------------------------------------------------------------------
// Global
// ----------------------------------------------------------------------------------------------------
const navigationTooltipIdCharacters = ['F', 'J', 'G', 'H', 'D', 'K', 'S', 'L', 'A'];
const MODE = {
    NORMAL: 0,
    LINK: 1,
    SEARCH: 2
};
const normalModeShortcut = 102; //f
const searchModeShortcut = 115; //s
const closeShortcut = 120; //x
const forwardHistoryShortcut = 76; //L
const selectNextTabShortcut = 75; //K
const selectPreviousTabShortcut = 74; //J
let mode = MODE.NORMAL;
let userInput;
let navigationTooltipAnchorDictionary = {};
let idCharacterIndex;

function initialize() {
    addKeyPressHandler();
    addSearchModalWindowHandlers();
    $(document).focus();
    console.debug("Keyboard support is ready");
}

// ----------------------------------------------------------------------------------------------------
// Handlers
// ----------------------------------------------------------------------------------------------------
function addKeyPressHandler() {
    $(document).keypress(function (event) {
        if (mode === MODE.NORMAL) {
            console.trace(event.which);

            if (event.which === normalModeShortcut) {
                switchMode(MODE.LINK);
            } else if (event.which === searchModeShortcut) {
                switchMode(MODE.SEARCH);
                event.preventDefault();
            } else if (event.which === closeShortcut) {
                window.close();
            } else if (event.which === selectNextTabShortcut) {
                selectTab('next');
            } else if (event.which === selectPreviousTabShortcut) {
                selectTab('previous');
            } else if (event.which === forwardHistoryShortcut) {
                chrome.tabs.goForward();
            }

        } else if (mode === MODE.LINK) {
            userInput += String.fromCharCode(event.which);
            filterNavigationTooltips(userInput.toUpperCase());

            const matchingLinksCount = $('.navigation-tooltip').length;
            if (matchingLinksCount === 0) {
                switchMode(MODE.NORMAL);
            } else if (matchingLinksCount === 1) {
                openLink();
                switchMode(MODE.NORMAL);
            }
        }
    });

    $(document).keyup(function (event) {
        if (mode === MODE.LINK && event.key === 'Escape') {
            switchMode(MODE.NORMAL);
        }
    });
}

function handleEnvironmentSwitch() {
    switchMode(MODE.NORMAL);
}

function switchMode(newMode) {
    if (mode === MODE.LINK) {
        removeAllNavigationTooltips();
    }

    if (newMode === MODE.LINK) {
        idCharacterIndex = 0;
        userInput = '';
        navigationTooltipAnchorDictionary = {};
        displayNavigationTooltips();
    } else if (newMode === MODE.SEARCH) {
        showSearchModalWindow();
    }
    mode = newMode;
}

// ----------------------------------------------------------------------------------------------------
// Navigation tooltips
// ----------------------------------------------------------------------------------------------------
function getNavigationTooltipIds() {
    const count = $('a').length;
    let ids = [];
    let maxLength = 0;
    for (let i = 0; i < count; i++) {
        const id = buildNavigationTooltipId(i);
        ids.push(id);

        if (id.length > maxLength) {
            maxLength = id.length;
        }
    }

    let alignedIds = [];
    for (let id of ids) {
        const length = id.length;
        if (length < maxLength) {
            for (let i = 0; i < (maxLength - length); i++) {
                id += navigationTooltipIdCharacters[0];
            }
        }
        alignedIds.push(id);
    }
    return alignedIds;
}

function buildNavigationTooltipId(n) {
    let s = navigationTooltipIdCharacters.length;
    let id = '';
    do {
        id += navigationTooltipIdCharacters[n % s];
        n = Math.floor(n / s);
    } while (n !== 0);

    return id;
}

function displayNavigationTooltips() {
    let navigationTooltipIds = getNavigationTooltipIds();

    $('a').not('.alert-link').each(function (index) {
        const anchor = $(this);
        const left = anchor.offset().left - 135;
        const top = anchor.offset().top - 2;
        const clazz = anchor.hasClass('environment-link') ? 'navigation-tooltip navigation-tooltip-environment' : 'navigation-tooltip';
        const id = navigationTooltipIds[index];

        let letteredId = '';
        $.each(id.split(''), function (index, character) {
            letteredId += `<span class="navigation-tooltip-inner-id-letter">${character}</span>`;
        });

        const navigationTooltip = $(
            `<div class="${clazz}"
                  style="left: ${left}px; top: ${top}px;"
                  id="${id}">
                 <span class="navigation-tooltip-inner">${letteredId}</span>
             </div>`);

        $('.navigation-tooltips-container').append(navigationTooltip);
        navigationTooltipAnchorDictionary[id] = anchor;
    });
}

function removeAllNavigationTooltips() {
    $('.navigation-tooltip').remove();
}

function filterNavigationTooltips(partialId) {
    $('.navigation-tooltip').each(function () {
        const navigationTooltip = $(this);
        const id = navigationTooltip.attr('id');
        if (!id.startsWith(partialId)) {
            navigationTooltip.remove();
        } else {
            const idLetters = navigationTooltip.find('.navigation-tooltip-inner').find('.navigation-tooltip-inner-id-letter');
            for (let i = 0; i < partialId.length; i++) {
                $(idLetters[i]).css('text-decoration', 'underline');
            }
        }
    });
}

function openLink() {
    const navigationTooltip = $('.navigation-tooltip');
    const inner = navigationTooltip.find('.navigation-tooltip-inner');
    const id = navigationTooltip.attr('id');
    const anchor = navigationTooltipAnchorDictionary[id];
    anchor[0].click();
    inner.addClass("navigation-tooltip-inner-opening");
}

// ----------------------------------------------------------------------------------------------------
// Search
// ----------------------------------------------------------------------------------------------------
function showSearchModalWindow() {
    $('#search-modal-window').modal('show');
}

function addSearchModalWindowHandlers() {
    const searchInput = $('#search-input');
    const searchModalWindow = $('#search-modal-window');
    searchModalWindow.on('hidden.bs.modal', function () {
        searchInput.val('');
        switchMode(MODE.NORMAL);
    });
    searchModalWindow.on('shown.bs.modal', function () {
        searchInput.focus();
    });

    searchInput.bind('submit', function () {
        const searchTerm = $(this).val();
        chrome.tabs.getCurrent(function (tab) {
            const searchUrl = "https://google.com/search?q=" + encodeURIComponent(searchTerm);
            searchInput.val('');
            chrome.tabs.update(tab.id, {url: searchUrl});
        });
    });
    searchInput.keyup(function (event) {
        if (event.keyCode === 13) {
            $(this).trigger("submit");
        }
    });
}

function selectTab(direction) {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        if (tabs.length) {
            const activeTab = tabs[0];
            const currentIndex = activeTab.index;
            chrome.tabs.query({currentWindow: true}, function (tabs) {
                const numTabs = tabs.length;
                let tabToActivateIndex = undefined;

                if (direction === 'next') {
                    tabToActivateIndex = (currentIndex + 1) % numTabs;
                } else if (direction === 'previous') {
                    tabToActivateIndex = currentIndex - 1;
                    if (tabToActivateIndex < 0) {
                        tabToActivateIndex = tabs.length - 1;
                    }
                }
                chrome.tabs.query({index: tabToActivateIndex}, function (tabs) {
                    if (tabs.length) {
                        const tabToActivate = tabs[0];
                        chrome.tabs.update(tabToActivate.id, {active: true});
                    }
                });
            });
        }
    });
}

export {initialize, handleEnvironmentSwitch};

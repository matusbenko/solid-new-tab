import {generateUuid} from "./uuid.js";
import {defaultSettings, defaultAppearanceSettings} from "./defaultSettings.js";

function migrate_v1_0_to_v1_1(callback) {
    chrome.storage.local.get('solidNewTabSettings', function (data) {

        // new settings format is not yet defined
        if (data.solidNewTabSettings === undefined) {
            chrome.storage.local.get(['content', 'keyboardSupport', 'environment', 'theme'], function (v1_0_settings) {
                let activeEnvironment = v1_0_settings.environment;

                // previous version settings are defined
                if (activeEnvironment !== undefined) {
                    let keyboardSupport = v1_0_settings.keyboardSupport;
                    let theme = v1_0_settings.theme;
                    let homeContent = v1_0_settings.content[0];
                    let workContent = v1_0_settings.content[1];

                    let v1_1_settings = defaultSettings;
                    v1_1_settings.keyboardSupport = keyboardSupport;
                    v1_1_settings.lastOpenNotification = 10000;
                    v1_1_settings.environment = [
                        {
                            id: generateUuid(),
                            name: 'Home',
                            icon: 'fas fa-home',
                            content: homeContent,
                            active: (activeEnvironment === 0),
                            appearance: defaultAppearanceSettings,
                            theme: theme
                        },
                        {
                            id: generateUuid(),
                            name: 'Work',
                            icon: 'fas fa-briefcase',
                            content: workContent,
                            active: (activeEnvironment === 1),
                            appearance: defaultAppearanceSettings,
                            theme: theme
                        }
                    ];

                    chrome.storage.local.set({solidNewTabSettings: v1_1_settings}, function () {
                        callback();
                    });
                } else {
                    callback();
                }
            });
        } else {
            callback();
        }
    });
}

export function loadSettings(callback) {
    migrate_v1_0_to_v1_1(function () {
        chrome.storage.local.get('solidNewTabSettings', function (data) {
            if (data.solidNewTabSettings === undefined) {
                resetSettings(callback);
            } else {
                callback();
            }
        });
    });
}

export function resetSettings(callback) {
    chrome.storage.local.set({solidNewTabSettings: defaultSettings}, function () {
        callback();
    });
}

export function colSpanValue2ColCount(colSpan) {
    switch (parseInt(colSpan)) {
        case 1:
            return 12;
        case 2:
            return 6;
        case 3:
            return 4;
        case 4:
            return 3;
        case 5:
            return 2;
        case 6:
            return 1;
    }
}



// ----------------------------------------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------------------------------------
function getSettings(callback) {
    chrome.storage.local.get('solidNewTabSettings', function (data) {
        callback(data.solidNewTabSettings);
    });
}

function setSettings(settings, callback) {
    chrome.storage.local.set({solidNewTabSettings: settings}, callback);
}

function setSetting(key, value, callback) {
    getSettings(function (settings) {
        settings[key] = value;
        setSettings(settings, callback);
    });
}

function getEnvironmentById(environmentId, callback) {
    getAllEnvironments(function (environments) {
        for (const environment of environments) {
            if (environment.id === environmentId) {
                callback(environment);
                break;
            }
        }
    });
}

function setEnvironmentSetting(environmentId, key, value, callback) {
    getSettings(function (settings) {
        for (const environment of settings.environment) {
            if (environment.id === environmentId) {
                environment[key] = value;
                break;
            }
        }
        setSettings(settings, callback);
    });
}

export function getAllEnvironments(callback) {
    getSettings(function (settings) {
        callback(settings.environment);
    });
}

export function setAllEnvironments(environments, callback) {
    setSetting("environment", environments, callback);
}

export function setActiveEnvironment(environmentId) {
    getAllEnvironments(function (environments) {
        for (const environment of environments) {
            environment.active = environment.id === environmentId;
        }
        setAllEnvironments(environments);
    });
}

export function getActiveEnvironment(callback) {
    getAllEnvironments(function (environments) {
        for (const environment of environments) {
            if (environment.active) {
                callback(environment);
                break;
            }
        }
    });
}

export function getContent(environmentId, callback) {
    getEnvironmentById(environmentId, function (environment) {
        callback(environment.content);
    });
}

export function setContent(environmentId, content) {
    setEnvironmentSetting(environmentId, "content", content);
}

export function getKeyboardSupport(callback) {
    getSettings(function (settings) {
        callback(settings.keyboardSupport);
    });
}

export function setKeyboardSupport(keyboardSupport) {
    setSetting("keyboardSupport", keyboardSupport);
}

export function getLastOpenNotification(callback) {
    getSettings(function (settings) {
        callback(settings.lastOpenNotification);
    });
}

export function setLastOpenNotification(notificationVersion) {
    setSetting("lastOpenNotification", notificationVersion);
}

export function getTheme(environmentId, callback) {
    getEnvironmentById(environmentId, function (environment) {
        callback(environment.theme);
    });
}

export function setTheme(environmentId, theme) {
    setEnvironmentSetting(environmentId, "theme", theme);
}

export function getAppearance(environmentId, callback) {
    getEnvironmentById(environmentId, function (environment) {
        callback(environment.appearance);
    });
}

export function setAppearance(environmentId, appearance) {
    setEnvironmentSetting(environmentId, "appearance", appearance);
}

export function resetAppearanceSettings(callback) {
    getAllEnvironments(function (environments) {
        for (const environment of environments) {
            environment.appearance = defaultAppearanceSettings;
        }
        setAllEnvironments(environments, callback);
    });
}

import {getTheme} from "./settings.js";

let appliedTheme;

function removeCurrentTheme() {
    $('link[bootstrapStyle=true]').remove();
}

function applyTheme(theme) {
    if (appliedTheme === theme) {
        return;
    }

    removeCurrentTheme();

    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        bootstrapStyle: true,
        href: `/css/customized-themes/${theme}.css`
    }).prependTo("head");
    appliedTheme = theme;
}

function setEnvironment(environmentId, callback) {
    getTheme(environmentId, (theme) => {
        applyTheme(theme);
        callback();
    });
}

export {applyTheme, setEnvironment};
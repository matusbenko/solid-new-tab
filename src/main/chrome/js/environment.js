import * as settings from "./settings.js";

function initialize(onEnvironmentSwitched) {
    createEnvironmentLinks(onEnvironmentSwitched);
}

function addEnvironmentSwitchContentHandler(onEnvironmentSwitched) {
    $('.environment a').click(function (event) {
        event.preventDefault();

        const environmentLinkId = $(this).attr('id');
        settings.setActiveEnvironment(environmentLinkId);
        onEnvironmentSwitched(environmentLinkId);
    });
}

function createEnvironmentLinks(onEnvironmentSwitched) {
    settings.getAllEnvironments(function (environments) {
        let footer = $('#footer');
        footer.empty();
        if (environments.length > 1) {
            for (const environment of environments) {
                footer.append(`<div class="environment">
                             <span class="icon-wrapper"><i class="${environment.icon}"> </i></span><a href="" class="environment-link" id="${environment.id}">${environment.name}</a>
                           </div>`);
            }
            addEnvironmentSwitchContentHandler(onEnvironmentSwitched)
        }
    });
}

export {initialize}

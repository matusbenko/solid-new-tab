import * as settings from "./settings.js";

let appearanceSettings;

function setEnvironment(environmentId, callback) {
    settings.getAppearance(environmentId, (appearance) => {
        appearanceSettings = appearance;
        settings.getContent(environmentId, (content) => {
            const syntaxErrorAlert = $('#syntax-error-alert');
            syntaxErrorAlert.css('display', 'none');

            try {
                drawContent(content);
                console.debug("Content is drawn");
            } catch (error) {
                syntaxErrorAlert.css('display', 'block');
            }

            callback();
        });
    });
}

function addColumn(colNum) {
    const colClass = 12 / settings.colSpanValue2ColCount(appearanceSettings.columns.colSpan);
    $('#row1').append(`
        <div class="col-${colClass} entry-column"
             id="col${colNum}"
             style=" padding: ${appearanceSettings.columns.padding.top}px ${appearanceSettings.columns.padding.right}px ${appearanceSettings.columns.padding.bottom}px ${appearanceSettings.columns.padding.left}px">
        </div>`);
}

function addTitle(colNum, title) {
    const sectionTitleSize = appearanceSettings.fontSize.sectionTitle;
    $('#col' + colNum).append(`<h${sectionTitleSize}>${title}</h${sectionTitleSize}>`);
}

function addLineSeparator(colNum) {
    $('#col' + colNum).append('<hr/>')
}

function addWidget(colNum, icon, text, link) {
    link = link.substr(1, link.length - 2);
    $('#col' + colNum).append(`
        <div class="entry">
            <div class="icon-wrapper">
                <i class="${icon}"> </i>
            </div>
            <div class="link-wrapper">
                ${text}
            </div>
        </div>
        <div class="entry">
            <div class="widget-wrapper">
                ${link}
            </div>
        </div> `);

}

function addLink(colNum, icon, text, link) {
    $('#col' + colNum).append(
        `<div class="entry">
             <div class="icon-wrapper">
                <i class="${icon}"> </i>
             </div>
             <div class="link-wrapper">
                <a href="${link}">${text}</a>
            </div>
        </div>`);
}

function addEntry(record, colNum) {
    const recordData = record.split(",");
    const icon = recordData[0].trim();
    const text = recordData[1].trim();
    let link = recordData[2].trim();
    if (link.startsWith('`')) {
        addWidget(colNum, icon, text, link);
    } else {
        addLink(colNum, icon, text, link);
    }
}

function drawContent(content) {
    if (content !== undefined) {
        let colNum = 1;
        addColumn(colNum);

        const records = content.split("\n");
        records.forEach(function (record) {
            if (record.length === 0) {
                addColumn(++colNum);
            } else if (record.startsWith('=')) {
                const title = record.substr(1, record.length - 1);
                addTitle(colNum, title);
            } else if (record === '-') {
                addLineSeparator(colNum);
            } else {
                addEntry(record, colNum);
            }
        });
    }
}

function cleanContent() {
    $('#row1').empty();
}

export {setEnvironment, cleanContent};
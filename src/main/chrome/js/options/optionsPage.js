import * as settings from "../settings.js";
import * as environmentTab from "./environmentTab.js";
import * as keyboardTab from "./keyboardTab.js";
import * as resetTab from "./resetTab.js";

$(function () {
    settings.loadSettings(function () {
        addTabHandlers();
        showRow("tab-environment");

        environmentTab.initialize();
        keyboardTab.initialize();
        resetTab.initialize();
    });
});

function addTabHandlers() {
    $('#optionsPageNavigation .nav-link').click(function () {
        let content = $(this).attr('content');
        showRow(content);
        $('#optionsPageNavigation .nav-link').removeClass('active');
        $(this).addClass('active');
    });
}

function showRow(content) {
    $('.optionsPageTab').css('display', 'none');
    let tabRow = $('#' + content + '.optionsPageTab');
    tabRow.css('display', 'flex');
}

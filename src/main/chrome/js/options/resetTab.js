import * as settings from "../settings.js";

function addResetAppearanceSettingsButtonClickedHandler() {
    $('button[name=resetAppearanceSettings]').click(function () {
        settings.resetAppearanceSettings(function () {
            location.reload();
        });
    });
}

function initialize() {
    addResetSettingsButtonClickedHandler();
    addResetAppearanceSettingsButtonClickedHandler();
}

function addResetSettingsButtonClickedHandler() {
    $('button[name=resetSettings]').click(function () {
        settings.resetSettings(function() {
            location.reload();
        });
    });
}

export {initialize};
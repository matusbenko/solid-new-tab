import * as environmentModal from "./environment/environmentModal.js";
import * as environmentRemoveConfirmationModal from "./environment/environmentRemoveConfirmationModal.js";
import * as environmentsTable from "./environment/environmentTable.js";
import * as environmentDetails from "./environment/environmentDetails.js";

function initialize() {
    environmentModal.initialize();
    environmentRemoveConfirmationModal.initialize();
    environmentDetails.initialize();
    environmentsTable.initialize(showEnvironmentDetails);
    addNewEnvironmentButtonClickedHandler();

}

function addNewEnvironmentButtonClickedHandler() {
    $('#newEnvironmentButton').click(function () {
        environmentModal.show();
    });
}

function showEnvironmentDetails(environmentId) {
    environmentDetails.setEnvironment(environmentId);
}

export {initialize};

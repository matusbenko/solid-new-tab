import {generateUuid} from "../../uuid.js";
import * as settings from "../../settings.js";
import {defaultAppearanceSettings} from "../../defaultSettings.js";

function initialize() {
    addFormSubmitHandler();
    addEnvironmentSaveButtonClickedHandler();
}

function show(id, icon, name) {
    let idField = $('#environmentFormModal #environmentIdField');
    idField.val('');
    if (id === undefined) {
        id = generateUuid();
    }
    idField.val(id);

    let iconField = $('#environmentFormModal #environmentIconField');
    iconField.val('');
    if (icon !== undefined) {
        iconField.val(icon);
    }

    let nameField = $('#environmentFormModal #environmentNameField');
    nameField.val('');
    if (name !== undefined) {
        nameField.val(name);
    }

    $('#environmentFormModal form').removeClass('was-validated');
    $('#environmentFormModal').modal('show');
}

function addEnvironmentSaveButtonClickedHandler() {
    $('#environmentSaveButton').click(function () {
        $('#environmentFormModal form').submit();
    });
}

function addFormSubmitHandler() {
    let environmentForm = $('#environmentFormModal form');
    environmentForm.submit(function (event) {
        let isValid = event.target.checkValidity();
        if (!isValid) {
            $(this).addClass('was-validated');
        } else {
            $(this).removeClass('was-validated');

            let idFieldValue = $('#environmentFormModal #environmentIdField').val();
            let iconFieldValue = $('#environmentFormModal #environmentIconField').val();
            let nameFieldValue = $('#environmentFormModal #environmentNameField').val();

            settings.getAllEnvironments(function (environments) {
                let isUpdated = false;
                for (let environment of environments) {
                    if (environment.id === idFieldValue) {
                        environment.icon = iconFieldValue;
                        environment.name = nameFieldValue;
                        isUpdated = true;
                    }
                }

                if (!isUpdated) {
                    environments.push({
                        id: idFieldValue,
                        name: nameFieldValue,
                        icon: iconFieldValue,
                        active: false,
                        appearance: defaultAppearanceSettings,
                        theme: 'default'
                    });
                }

                settings.setAllEnvironments(environments, function() {
                    $('#environmentFormModal').modal('hide');
                });
            });
        }
        event.preventDefault();
        event.stopPropagation();
    });
}

export {initialize, show};
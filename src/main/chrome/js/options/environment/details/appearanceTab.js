import * as backgroundSection from "./appearance/backgroundSection.js";
import * as fontSizeSection from "./appearance/fontSizeSection.js";
import * as columnsSection from "./appearance/columnsSection.js";

function initialize() {
    backgroundSection.initialize();
    fontSizeSection.initialize();
    columnsSection.initialize();
}

function setEnvironment(environmentId) {
    backgroundSection.setEnvironment(environmentId);
    fontSizeSection.setEnvironment(environmentId);
    columnsSection.setEnvironment(environmentId);
}

export {initialize, setEnvironment};
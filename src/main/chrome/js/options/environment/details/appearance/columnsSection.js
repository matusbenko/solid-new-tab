import * as settings from "../../../../settings.js";

let selectedEnvironmentId;

function updateColSpanIndicator() {
    const item = $('#columnGridColSpan');
    $('#columnGridColSpanIndicator').text(settings.colSpanValue2ColCount(item.val()));
}

function addColSpanItemValueChangedHandler() {
    const item = $('#columnGridColSpan');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.columns.colSpan = item.val();
            updateColSpanIndicator();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeColSpanItem() {
    const item = $('#columnGridColSpan');
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.columns.colSpan);
        updateColSpanIndicator();
    });
}

function initializePaddingItems() {
    ['Left', 'Top', 'Right', 'Bottom'].forEach(Property => initializePaddingItem(Property));
}

function initializePaddingItem(Property) {
    const item = $('#columnPadding' + Property);
    const property = Property.toLowerCase();

    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.columns.padding[property]);
    });
}

function addPaddingItemsValueChangedHandler() {
    ['Left', 'Top', 'Right', 'Bottom'].forEach(Property => addPaddingItemValueChangedHandler(Property));
}

function addPaddingItemValueChangedHandler(Property) {
    const item = $('#columnPadding' + Property);
    const property = Property.toLowerCase();

    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.columns.padding[property] = item.val();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeVerticalAlignmentItem() {
    const item = $('#columnsVerticalAlignmentItem');
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.columns.verticalAlignment);
    });
}

function addVerticalAlignmentItemValueChangedHandler() {
    const item = $('#columnsVerticalAlignmentItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.columns.verticalAlignment = item.val();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function addHorizontalAlignmentItemValueChangedHandler() {
    const item = $('#columnsHorizontalAlignmentItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.columns.horizontalAlignment = item.val();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeHorizontalAlignmentItem() {
    const item = $('#columnsHorizontalAlignmentItem');
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.columns.horizontalAlignment);
    });
}

function initialize() {
    addColSpanItemValueChangedHandler();
    addPaddingItemsValueChangedHandler();
    addVerticalAlignmentItemValueChangedHandler();
    addHorizontalAlignmentItemValueChangedHandler();
}

function setEnvironment(environmentId) {
    selectedEnvironmentId = environmentId;
    initializeColSpanItem();
    initializePaddingItems();
    initializeVerticalAlignmentItem();
    initializeHorizontalAlignmentItem();
}

export {initialize, setEnvironment};

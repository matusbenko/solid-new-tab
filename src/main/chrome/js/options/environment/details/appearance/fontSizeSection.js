import * as settings from "../../../../settings.js";

let selectedEnvironmentId;

function getSectionTitleSizeItemValue() {
    const itemValue = $('#sectionTitleSizeItem').val();
    // Transform 0..5 to 6..1
    return 6 - itemValue;
}

function setSectionTitleSizeItemValue(headerLevel) {
    $('#sectionTitleSizeItem').val(6 - headerLevel);
}

function updateSectionTitleSizeIndicator() {
    $('#sectionTitleSizeIndicator').text('h' + getSectionTitleSizeItemValue());
}

function addSectionTitleSizeItemValueChangedHandler() {
    const item = $('#sectionTitleSizeItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            updateSectionTitleSizeIndicator();
            appearance.fontSize.sectionTitle = getSectionTitleSizeItemValue();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeSectionTitleSizeItem() {
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        const item = $('#sectionTitleSizeItem');
        setSectionTitleSizeItemValue(appearance.fontSize.sectionTitle);
        updateSectionTitleSizeIndicator();
        addSectionTitleSizeItemValueChangedHandler();
        item.on('input', updateSectionTitleSizeIndicator);
    });
}


function getLinkSizeItemValue() {
    return Number($('#linkSizeItem').val()).toFixed(3);
}

function updateLinkSizeIndicator() {
    $('#linkSizeIndicator').text(getLinkSizeItemValue() + 'rem');
}

function addLinkSizeItemValueChangedHandler() {
    const item = $('#linkSizeItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            updateLinkSizeIndicator();
            appearance.fontSize.link = item.val();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeLinkSizeItem() {
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        let item = $('#linkSizeItem');
        item.val(appearance.fontSize.link);
        updateLinkSizeIndicator();
        item.on('input', updateLinkSizeIndicator);
    });
}

function initialize() {
    addLinkSizeItemValueChangedHandler();
    addSectionTitleSizeItemValueChangedHandler
}

function setEnvironment(environmentId) {
    selectedEnvironmentId = environmentId;
    initializeSectionTitleSizeItem();
    initializeLinkSizeItem();
}

export {initialize, setEnvironment};

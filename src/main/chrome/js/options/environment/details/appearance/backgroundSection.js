import * as settings from "../../../../settings.js";

let selectedEnvironmentId;

function addBackgroundImageUrlValueChangedHandler() {
    const item = $('#backgroundImageUrlItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.background.imageUrl = item.val();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function initializeBackgroundImageUrlItem() {
    const item = $('#backgroundImageUrlItem');
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.background.imageUrl);
    });
}

function addBackgroundImageOpacityValueChangedHandler() {
    const item = $('#backgroundImageOpacityItem');
    item.change(function () {
        settings.getAppearance(selectedEnvironmentId, function (appearance) {
            appearance.background.opacity = item.val();
            updateBackgroundOpacityIndicator();
            settings.setAppearance(selectedEnvironmentId, appearance);
        });
    });
}

function updateBackgroundOpacityIndicator() {
    $('#backgroundImageOpacityIndicator').text($('#backgroundImageOpacityItem').val());
}

function initializeBackgroundOpacityItem() {
    const item = $('#backgroundImageOpacityItem');
    settings.getAppearance(selectedEnvironmentId, function (appearance) {
        item.val(appearance.background.opacity);
        updateBackgroundOpacityIndicator();
        item.on("input", updateBackgroundOpacityIndicator);
    });
}

function initialize() {
    addBackgroundImageUrlValueChangedHandler();
    addBackgroundImageOpacityValueChangedHandler();
}

function setEnvironment(environmentId) {
    selectedEnvironmentId = environmentId;
    initializeBackgroundImageUrlItem();
    initializeBackgroundOpacityItem();
}

export {initialize, setEnvironment};
import * as settings from "../../../settings.js";

let selectedEnvironmentId;

function initialize() {
    addApplyThemeButtonClicked();
}

function initializeTheme() {
    resetApplyThemeButtons();
    settings.getTheme(selectedEnvironmentId, function (theme) {
        markThemeApplied(theme);
    });
}

function resetApplyThemeButtons() {
    let buttons = $('.card .btn');
    buttons.text('Apply theme');
    buttons.removeClass('btn-success');
}

function markThemeApplied(theme) {
    let button = $('.card[theme='+theme+'] button');
    button.addClass('btn-success');
    button.text('Applied');
}

function addApplyThemeButtonClicked() {
    $('.card .btn').click(function () {
        resetApplyThemeButtons();

        let button = $(this);
        let title = button.prev();
        const theme = title.text().toLowerCase();
        markThemeApplied(theme);
        saveThemeSetting(theme);
    });
}


function saveThemeSetting(theme) {
    settings.setTheme(selectedEnvironmentId, theme);
}

function setEnvironment(environmentId) {
    selectedEnvironmentId = environmentId;
    initializeTheme();
}
export {initialize, setEnvironment};
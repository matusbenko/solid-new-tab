import * as settings from "../../../settings.js";

let selectedEnvironmentId;

function initialize() {
    addContentTextAreaValueChangedHandler();
}

function addContentTextAreaValueChangedHandler() {
    $('textarea[name=content]').bind('input propertychange', function () {
        saveContentForSelectedEnvironment();
    });
}

function saveContentForSelectedEnvironment() {
    const textarea = $('textarea[name=content]');
    const content = textarea.val();
    settings.setContent(selectedEnvironmentId, content);
}

function initializeTextArea(environmentId) {
    const textarea = $('textarea[name=content]');
    settings.getContent(environmentId, function (content) {
        textarea.val(content);
    });
}

function setEnvironment(environmentId) {
    selectedEnvironmentId = environmentId;
    initializeTextArea(environmentId);
}

export {initialize, setEnvironment};
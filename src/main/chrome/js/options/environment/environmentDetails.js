import * as contentTab from "./details/contentTab.js";
import * as themeTab from "./details/themeTab.js";
import * as appearanceTab from "./details/appearanceTab.js";

function initialize() {
    addTabHandlers();
    contentTab.initialize();
    themeTab.initialize();
    appearanceTab.initialize();
}

function setEnvironment(environmentId) {
    if (environmentId !== undefined) {
        contentTab.setEnvironment(environmentId);
        themeTab.setEnvironment(environmentId);
        appearanceTab.setEnvironment(environmentId);
        $('#environmentDetails').css('display', 'flex');
    } else {
        $('#environmentDetails').css('display', 'none');
    }
}

function addTabHandlers() {
    $('#environmentDetailsNavigation .nav-link').click(function () {
        let content = $(this).attr('content');
        showRow(content);
        $('#environmentDetailsNavigation .nav-link').removeClass('active');
        $(this).addClass('active');
    });
}

function showRow(content) {
    $('.environmentDetailsTab').css('display', 'none');
    let tabRow = $('#' + content + '.environmentDetailsTab');
    tabRow.css('display', 'flex');
}

export {initialize, setEnvironment};
import * as environmentModal from "./environmentModal.js";
import * as environmentRemoveConfirmationModal from "./environmentRemoveConfirmationModal.js";
import * as settings from "../../settings.js";

function initialize(onEnvironmentSelected) {
    createTable(onEnvironmentSelected);
    addEnvironmentModalHideHandler();
    addEnvironmentRemoveConfirmationModalHideHandler();
    fetchTableData();
}

function selectActiveEnvironment() {
    const table = $('#environments-table');
    const environments = table.bootstrapTable('getData');

    for (const [index, environment] of environments.entries()) {
        if (environment.active) {
            table.bootstrapTable('check', index);
            break;
        }
    }
}

function fetchTableData() {
    const table = $('#environments-table');

    settings.getAllEnvironments(function (environments) {
        table.bootstrapTable('load', environments);

        if (table.bootstrapTable('getSelections').length === 0) {
            selectActiveEnvironment();
        }
    });
}

function addEnvironmentModalHideHandler() {
    $('#environmentFormModal').on('hide.bs.modal', function () {
        fetchTableData();
    });
}

function addEnvironmentRemoveConfirmationModalHideHandler() {
    $('#environmentRemoveConfirmationModal').on('hide.bs.modal', function () {
        fetchTableData();
    });
}

function createTable(onEnvironmentSelected) {
    $('#environments-table').bootstrapTable({
        theadClasses: 'thead-light',
        useRowAttrFunc: "true",
        reorderableRows: true,
        singleSelect: true,
        columns: [
            {
                field: 'state',
                radio: true,
            },
            {
                field: 'id',
                visible: false
            },
            {
                field: "icon",
                title: "Icon",
                width: 20,
                widthUnit: 'px',
                align: 'center',
                formatter: iconFormatter
            },
            {
                field: 'name',
                title: 'Name',
            },
            {
                field: 'operations',
                title: '',
                width: 200,
                widthUnit: 'px',
                align: 'center',
                events: {
                    'click .editEnvironment': function (e, value, row) {
                        environmentModal.show(row.id, row.icon, row.name);
                    },
                    'click .removeEnvironment': function (e, value, row) {
                        environmentRemoveConfirmationModal.show(row.id);
                    }
                },
                formatter: operationsFormatter
            }
        ],
        onReorderRow: onReorderRowHandler,
        onCheck: function (row) {
            onEnvironmentSelected(row.id);
        }
    });
}

function onReorderRowHandler(rows) {
    settings.getAllEnvironments(function (environments) {
        let environmentsNewOrder = [];
        for (const row of rows) {
            for (const environment of environments) {
                if (row.id === environment.id) {
                    environmentsNewOrder.push(environment);
                    break;
                }
            }
        }
        settings.setAllEnvironments(environmentsNewOrder);
    });
}

function operationsFormatter() {
    let isLastEnvironment = $('#environments-table').bootstrapTable('getData').length === 1;
    if (isLastEnvironment) {
        return `
        <button class="btn btn-warning editEnvironment" title="Edit"><i class="fa fa-pen"></i></button>
        <button class="btn btn-danger removeEnvironment" title="Last environment can't be removed" disabled><i class="fa fa-trash"></i></button>
    `;
    } else {
        return `
        <button class="btn btn-warning editEnvironment" title="Edit"><i class="fa fa-pen"></i></button>
        <button class="btn btn-danger removeEnvironment" title="Remove"><i class="fa fa-trash"></i></button>
    `;
    }
}

function iconFormatter(value) {
    return `<i class="${value}"></i>`;
}

export {initialize};

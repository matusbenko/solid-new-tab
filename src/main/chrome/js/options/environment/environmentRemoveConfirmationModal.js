import * as settings from "../../settings.js";

let environmentToRemoveId;

function initialize() {
    addEnvironmentRemoveConfirmButtonClickedHandler();
}

function addEnvironmentRemoveConfirmButtonClickedHandler() {
    $('#environmentRemoveConfirmButton').click(function () {
        settings.getAllEnvironments(function (environments) {
            let environmentsAfterRemoval = [];
            let environmentToRemoveIsActive = false;
            for (const environment of environments) {
                if (environment.id !== environmentToRemoveId) {
                    environmentsAfterRemoval.push(environment);
                } else {
                    environmentToRemoveIsActive = environment.active;
                }
            }

            if (environmentToRemoveIsActive) {
                environmentsAfterRemoval[0].active = true;
            }

            settings.setAllEnvironments(environmentsAfterRemoval, function () {
                $('#environmentRemoveConfirmationModal').modal('hide');
            });
        });
    });
}

function show(id) {
    environmentToRemoveId = id;
    $('#environmentRemoveConfirmationModal').modal('show');
}

export {initialize, show};
import * as settings from "../settings.js";

function initialize() {
    initializeKeyboardSupport();
    addKeyboardSupportCheckboxValueChangedHandler();
}

function addKeyboardSupportCheckboxValueChangedHandler() {
    $('input[type=checkbox][name=keyboardSupport]').change(function () {
        const keyboardSupport = this.checked;
        saveKeyboardSupportSetting(keyboardSupport);
    });
}

function saveKeyboardSupportSetting(keyboardSupport) {
    settings.setKeyboardSupport(keyboardSupport);
}

function initializeKeyboardSupport() {
    settings.getKeyboardSupport(function (keyboardSupport) {
        $('input[type=checkbox][name=keyboardSupport]').prop('checked', keyboardSupport);
    });
}

export {initialize};
import * as settings from './settings.js';
import * as keyboard from "./keyboard.js";
import * as theme from "./theme.js";
import * as content from "./content.js";
import * as appearance from "./appearance.js";
import * as environment from "./environment.js";
import * as controls from "./controls.js";

function onContentLoaded() {
    $('body').removeClass().addClass('loaded');
}

function initialize(keyboardSupport) {
    controls.initialize();
    environment.initialize(setEnvironment);
    if (keyboardSupport) {
        keyboard.initialize();
    }
}

function setEnvironment(environmentId) {
    $('body').removeClass().addClass("loading");
    keyboard.handleEnvironmentSwitch();
    content.cleanContent();
    theme.setEnvironment(environmentId, () => {
        appearance.setEnvironment(environmentId, () => {
            content.setEnvironment(environmentId, onContentLoaded);
        });
    });
}

function main() {
    settings.loadSettings(function () {
        settings.getActiveEnvironment(function (environment) {
            theme.applyTheme(environment.theme);
            settings.getKeyboardSupport(function (keyboardSupport) {
                if (keyboardSupport && window.location.hash === '#newTab') {
                    chrome.tabs.create({url: chrome.extension.getURL("index.html")});
                    window.close();
                } else {
                    initialize(keyboardSupport);
                    setEnvironment(environment.id);
                }
            });
        });
    });
}

main();
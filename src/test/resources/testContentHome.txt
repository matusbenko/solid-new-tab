=Tools
fab fa-google, Google, https://google.com
fas fa-search, Duck Duck Go, https://duckduckgo.com/
fas fa-spell-check, Dictionary, https://www.dictionary.com/
-
fas fa-language, Translate, https://translate.google.com/
fab fa-youtube, YouTube, http://youtube.com
fas fa-map, Maps, https://www.google.com/maps
fab fa-google-drive, Drive, https://drive.google.com/drive/u/0/my-drive
-
fas fa-truck, IP address, `<iframe src="https://api.ipify.org/?format=json" style="border:0; height:40px;"></iframe>`

=Programming
fab fa-gitlab, GitLab, https://gitlab.com/explore/projects/trending
fab fa-github, GitHub, https://github.com/explore
fab fa-bitbucket, BitBucket, https://bitbucket.org
=Social
fab fa-mastodon, Mastodon, https://mastodon.social
fab fa-reddit, Reddit, https://reddit.com

=News
fas fa-newspaper, DW, https://www.dw.com/en/top-stories/s-9097
fas fa-newspaper, RT, https://www.rt.com/
fas fa-newspaper, France24, https://www.france24.com/en/
fas fa-newspaper, BBC, https://www.bbc.com/
fas fa-newspaper, CNN, https://edition.cnn.com/
fas fa-newspaper, CGTN, https://www.cgtn.com/
-
fas fa-rss, Medium, https://medium.com/
fas fa-rss, It's FOSS, https://itsfoss.com/
fas fa-rss, opensource.com, https://opensource.com/
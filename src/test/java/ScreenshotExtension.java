import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class ScreenshotExtension implements AfterTestExecutionCallback {

    public static final String PATH = "/usr/screenshots";
    private TakesScreenshot driver;

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        Boolean isRunningInDocker = Boolean.valueOf(System.getProperty("docker"));
        if (!isRunningInDocker) {
            System.out.println("Docker not detected, not saving screenshot");
            return;
        }

        Object test = extensionContext.getRequiredTestInstance();
        Field a = test.getClass().getSuperclass().getDeclaredField("driver");
        a.setAccessible(true);
        driver = (TakesScreenshot) a.get(test);

        Class<?> clazz = extensionContext.getRequiredTestClass();
        Method method = extensionContext.getRequiredTestMethod();
        if (extensionContext.getExecutionException().isPresent()) {
            makeScreenshot(String.format("%s-%s", clazz.getName(), method.getName()));
        }
    }

    private void makeScreenshot(String fileName) {
        File scrFile = driver.getScreenshotAs(OutputType.FILE);
        try {
            String pathname = PATH + "/" + fileName + ".png";
            System.out.println("Saving screenshot " + pathname);
            FileUtils.copyFile(scrFile, new File(pathname));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

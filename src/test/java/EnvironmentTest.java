import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

class EnvironmentTest extends ExtensionLoader {
    @Test
    void switchingFromHomeToWork() {
        openNewTab();

        setEnvironment(Environment.home);

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("News"));
        Assertions.assertFalse(bodyText.contains("Knowledge"));

        setEnvironment(Environment.work);

        bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertFalse(bodyText.contains("News"));
        Assertions.assertTrue(bodyText.contains("Knowledge"));
    }

    @Test
    void createEnvironment() throws InterruptedException {
        openOptions("tab-environment");

        driver.findElementByCssSelector("#tab-environment #newEnvironmentButton").click();
        waitForFormModalToOpen();
        driver.findElementByCssSelector("#environmentIconField").sendKeys("fas fa-adjust");
        driver.findElementByCssSelector("#environmentNameField").sendKeys("Foo");
        Thread.sleep(100);
        driver.findElementByCssSelector("#environmentSaveButton").click();
        waitForFormModalToClose();

        List<WebElement> rows = driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr");
        Assertions.assertTrue(rows.get(0).getText().contains("Home"));
        Assertions.assertTrue(rows.get(1).getText().contains("Work"));
        Assertions.assertTrue(rows.get(2).getText().contains("Foo"));

        openNewTab();
        List<WebElement> environmentLinks = driver.findElementsByCssSelector(".environment-link");
        Assertions.assertTrue(environmentLinks.get(0).getText().equals("Home"));
        Assertions.assertTrue(environmentLinks.get(1).getText().equals("Work"));
        Assertions.assertTrue(environmentLinks.get(2).getText().equals("Foo"));
    }

    private void waitForFormModalToOpen() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(webDriver -> webDriver.findElement(By.cssSelector("#environmentFormModal")).isDisplayed());
    }

    private void waitForFormModalToClose() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(webDriver -> !webDriver.findElement(By.cssSelector("#environmentFormModal")).isDisplayed());
    }

    @Test
    void environmentValidation() throws InterruptedException {
        openOptions("tab-environment");

        driver.findElementByCssSelector("#tab-environment #newEnvironmentButton").click();
        waitForFormModalToOpen();
        driver.findElementByCssSelector("#environmentSaveButton").click();
        Thread.sleep(100);
        Assertions.assertTrue(driver.findElementByCssSelector("#environmentFormModal .invalid-feedback").isDisplayed());

        driver.findElementByCssSelector("#environmentFormModal button[data-dismiss=modal]").click();
        waitForFormModalToClose();

        driver.findElementByCssSelector("#tab-environment #environments-table tbody tr .editEnvironment").click();
        waitForFormModalToOpen();
        driver.findElementByCssSelector("#environmentNameField").clear();
        driver.findElementByCssSelector("#environmentSaveButton").click();
        Thread.sleep(100);
        Assertions.assertTrue(driver.findElementByCssSelector("#environmentFormModal .invalid-feedback").isDisplayed());
    }

    @Test
    void editEnvironment() throws InterruptedException {
        openOptions("tab-environment");

        driver.findElementByCssSelector("#tab-environment #environments-table tbody tr .editEnvironment").click();
        waitForFormModalToOpen();
        driver.findElementByCssSelector("#environmentIconField").clear();
        driver.findElementByCssSelector("#environmentIconField").sendKeys("fas fa-adjust");
        driver.findElementByCssSelector("#environmentNameField").clear();
        driver.findElementByCssSelector("#environmentNameField").sendKeys("Foo");
        Thread.sleep(100);
        driver.findElementByCssSelector("#environmentSaveButton").click();
        waitForFormModalToClose();

        List<WebElement> rows = driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr");
        Assertions.assertTrue(rows.get(0).getText().contains("Foo"));
        Assertions.assertTrue(rows.get(1).getText().contains("Work"));

        openNewTab();
        List<WebElement> environmentLinks = driver.findElementsByCssSelector(".environment-link");
        Assertions.assertTrue(environmentLinks.get(0).getText().equals("Foo"));
        Assertions.assertTrue(environmentLinks.get(1).getText().equals("Work"));
    }

    @Test
    void removeActiveEnvironment() {
        openOptions("tab-environment");

        driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr .removeEnvironment").get(1).click();
        waitForConfirmationModalToOpen();
        driver.findElementByCssSelector("#environmentRemoveConfirmButton").click();
        waitForConfirmationModalToClose();

        List<WebElement> rows = driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr");
        Assertions.assertEquals(1, rows.size());
        Assertions.assertTrue(rows.get(0).getText().contains("Home"));
        Assertions
            .assertFalse(driver.findElementByCssSelector("#tab-environment #environments-table tbody tr .removeEnvironment").isEnabled());
        Assertions.assertTrue(driver.findElementByCssSelector(OPTION_TEXTAREA_CONTENT).isDisplayed(),
            "Details should be visible for next environment after active environment was delete");

        openNewTab();
        List<WebElement> environmentLinks = driver.findElementsByCssSelector(".environment-link");
        Assertions.assertEquals(0, environmentLinks.size());
        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("News"));
        Assertions.assertFalse(bodyText.contains("Knowledge"));
    }

    private void waitForConfirmationModalToOpen() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(webDriver -> webDriver.findElement(By.cssSelector("#environmentRemoveConfirmationModal")).isDisplayed());
    }

    private void waitForConfirmationModalToClose() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(webDriver -> !webDriver.findElement(By.cssSelector("#environmentRemoveConfirmationModal")).isDisplayed());
    }

    @Test
    void reorderEnvironments() {
        // drag and drop doesn't work for me
    }

    @Test
    void optionsPageEnvironmentTab() {
        openOptions("tab-environment");

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains(
            "Environments allow to specify different new tab pages for different situations, for example one for work and another for home.\n"));
        Assertions.assertTrue(bodyText.contains("Rows in the table can be dragged to change the order of the environments.\n"));
        Assertions.assertTrue(
            bodyText.contains("If only one environment is specified, new tab page will not show the \"switcher\" at the page bottom."));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#newEnvironmentButton")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#environments-table")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#environments-table i.fas.fa-home")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#environments-table i.fas.fa-briefcase")).size());
        Assertions.assertEquals(2, driver.findElements(By.cssSelector("#environments-table .editEnvironment")).size());
        Assertions.assertEquals(2, driver.findElements(By.cssSelector("#environments-table .removeEnvironment")).size());

    }
}

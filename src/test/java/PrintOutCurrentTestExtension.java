import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class PrintOutCurrentTestExtension implements BeforeEachCallback {
    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        extensionContext.getTestMethod().ifPresent(method -> {
            System.out.println();
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println(method.getName());
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
        });
    }
}

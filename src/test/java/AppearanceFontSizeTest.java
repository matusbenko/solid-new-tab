import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

public class AppearanceFontSizeTest extends ExtensionLoader {
    @Test
    void sectionTitleSize() {
        openNewTab();
        String setSectionTitleSizeScript = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.fontSize.sectionTitle=1;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(setSectionTitleSizeScript);
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        Assertions.assertEquals("h1", driver.findElementByCssSelector("#sectionTitleSizeIndicator").getText());
        Assertions.assertEquals("5", driver.findElementByCssSelector("#sectionTitleSizeItem").getAttribute("value"));

        openNewTab();
        Assertions.assertTrue(driver.findElementsByCssSelector("h3").isEmpty());
        Assertions.assertFalse(driver.findElementsByCssSelector("h1").isEmpty());
    }

    @Test
    void linkSize() {
        openNewTab();
        String setLinkSizeScript = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.fontSize.link=1;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(setLinkSizeScript);
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        Assertions.assertEquals("1.000rem", driver.findElementByCssSelector("#linkSizeIndicator").getText());
        Assertions.assertEquals("1", driver.findElementByCssSelector("#linkSizeItem").getAttribute("value"));

        openNewTab();
        Assertions.assertEquals("font-size: 1rem;", driver.findElementByTagName("body").getAttribute("style"));
    }

    @Test
    void optionsPageAppearanceTabFontSizeSection() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Font size"));
        Assertions.assertTrue(bodyText.contains("Section title"));
        Assertions.assertTrue(bodyText.contains("Links"));
        Assertions.assertTrue(bodyText.contains("h3"));
        Assertions.assertTrue(bodyText.contains("0.875rem"));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#sectionTitleSizeIndicator")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#sectionTitleSizeItem")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#linkSizeIndicator")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#linkSizeItem")).size());

    }
}


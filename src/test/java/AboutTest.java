import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AboutTest extends ExtensionLoader {
    @Test
    void optionsPageAboutTab() {
        openOptions("tab-about");

        Assertions.assertEquals(1, driver.findElementsByCssSelector("#tab-about .card").size());
        Assertions.assertEquals("Solid New Tab", driver.findElementByCssSelector("#tab-about .card h5").getText());
        Assertions.assertEquals("v1.1", driver.findElementByCssSelector("#tab-about .card p small").getText());

        List<String> cardLinkTextList = driver.findElementsByCssSelector("#tab-about .card p a").stream().map(WebElement::getText)
            .collect(Collectors.toList());
        Assertions.assertTrue(cardLinkTextList.contains("Code repository"));
        Assertions.assertTrue(cardLinkTextList.contains("Chrome Web Store page"));
        Assertions.assertTrue(cardLinkTextList.contains("u/SolidNewTabDev"));

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Share your own customized start page with others on /r/startpages."));
        Assertions
            .assertTrue(bodyText.contains("Your feedback is very welcomed. This is an open source extension you can also contribute to"));
        Assertions.assertTrue(bodyText.contains("If you want to support you can"));

        Assertions.assertEquals(1, driver.findElementsByCssSelector("#tab-about .bmc-button").size());
    }

    @Test
    void buyMeACoffeeButton() {
        openOptions("tab-about");
        String optionsPageWindowHandle = driver.getWindowHandle();
        driver.findElementByCssSelector("#tab-about .bmc-button").click();

        Set<String> windowHandles = driver.getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(optionsPageWindowHandle)) {
                driver.switchTo().window(windowHandle);
            }
        }

        Assertions.assertEquals("https://www.buymeacoffee.com/0lqAi6k", driver.getCurrentUrl());
    }
}

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

class ThemeTest extends ExtensionLoader {
    private final String[] themes = new String[] { "cerulean", "cosmo", "cyborg", "darkly", "default", "flatly", "journal", "litera",
        "lumen", "lux", "materia", "minty", "pulse", "sandstone", "simplex", "sketchy", "slate", "solar", "spacelab", "superhero", "united",
        "yeti" };

    @Test
    void ifDefaultThemeIsSet() {
        openNewTab();
        assertThemeApplied("default");
    }

    private void assertThemeApplied(String theme) {
        List<WebElement> cssLinks = driver.findElements(By.cssSelector("link[bootstrapstyle=true]"));
        Assertions.assertEquals(1, cssLinks.size());
        Assertions.assertTrue(cssLinks.get(0).getAttribute("href").contains(getCssPath(theme)));
    }

    private String getCssPath(String theme) {
        return "/css/customized-themes/" + theme + ".css";
    }

    @Test
    void ifChangingThemeAppliesIt() throws InterruptedException {
        for (String theme : themes) {
            openOptions("tab-environment");
            openEnvironmentDetails("tab-environment-theme");
            selectTheme(theme);
            openNewTab();
            assertThemeApplied(theme);
        }

    }

    private void selectTheme(String theme) throws InterruptedException {
        WebElement themeApplyButton = driver.findElementByCssSelector(".card[theme=" + theme + "] .card-body button");
        // lowerCase comparison because one theme transfers button texts to upper case
        Assertions.assertEquals("apply theme", themeApplyButton.getText().toLowerCase());
        themeApplyButton.click();
        Thread.sleep(20);
        Assertions.assertEquals("applied", themeApplyButton.getText().toLowerCase());
    }

    @Test
    void ifStoredThemeIsApplied() throws InterruptedException {
        String theme = themes[0];
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-theme");
        selectTheme(theme);

        openOtherPage();
        openNewTab();
        assertThemeApplied(theme);
    }

    @Test
    void optionsPageThemeTab() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-theme");
        for (String theme : themes) {
            String message = "Test of "+theme;
            Assertions.assertEquals(1, driver.findElementsByCssSelector(".card[theme="+theme+"]").size(), message);
            Assertions.assertEquals(1, driver.findElementsByCssSelector(".card[theme="+theme+"] img[src='images/theme-"+theme+".png']").size(), message);
            String themeTitle = theme.substring(0, 1).toUpperCase() + theme.substring(1);
            Assertions.assertEquals(themeTitle,  driver.findElementByCssSelector(".card[theme="+theme+"] .card-body .card-title").getText(), message);
            Assertions.assertEquals(1, driver.findElementsByCssSelector(".card[theme="+theme+"] .card-body button").size(), message);
        }

    }
}

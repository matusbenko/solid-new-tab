import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AppearanceColumnsTest extends ExtensionLoader {
    @Test
    void columnsCount() {
        openNewTab();
        String script = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.columns.colSpan=1;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(script);
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        Assertions.assertEquals("12", driver.findElementByCssSelector("#columnGridColSpanIndicator").getText());
        Assertions.assertEquals("1", driver.findElementByCssSelector("#columnGridColSpan").getAttribute("value"));

        openNewTab();
        Assertions.assertEquals(3, driver.findElementsByCssSelector(".col-1").size());
    }

    @Test
    void columnsDistribution() {
        openNewTab();
        String script = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.columns.verticalAlignment='align-items-end';
                data.solidNewTabSettings.environment[1].appearance.columns.horizontalAlignment='justify-content-end';
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(script);
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        Assertions.assertEquals("align-items-end", driver.findElementByCssSelector("#columnsVerticalAlignmentItem").getAttribute("value"));
        Assertions.assertEquals("justify-content-end", driver.findElementByCssSelector("#columnsHorizontalAlignmentItem").getAttribute("value"));

        openNewTab();
        String rowClasses = driver.findElementById("row1").getAttribute("class");
        Assertions.assertEquals("row align-items-end justify-content-end", rowClasses);
    }

    @Test
    void columnsPadding() {
        openNewTab();
        String script = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.columns.padding.left=1;
                data.solidNewTabSettings.environment[1].appearance.columns.padding.top=2;
                data.solidNewTabSettings.environment[1].appearance.columns.padding.right=3;
                data.solidNewTabSettings.environment[1].appearance.columns.padding.bottom=4;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(script);
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        Assertions.assertEquals("1", driver.findElementByCssSelector("#columnPaddingLeft").getAttribute("value"));
        Assertions.assertEquals("2", driver.findElementByCssSelector("#columnPaddingTop").getAttribute("value"));
        Assertions.assertEquals("3", driver.findElementByCssSelector("#columnPaddingRight").getAttribute("value"));
        Assertions.assertEquals("4", driver.findElementByCssSelector("#columnPaddingBottom").getAttribute("value"));

        openNewTab();
        List<WebElement> columns = driver.findElementsByClassName("entry-column");
        for (WebElement column : columns) {
            String padding = column.getCssValue("padding");
            Assertions.assertEquals("2px 3px 4px 1px", padding);
        }
    }

    @Test
    void optionsPageAppearanceTabColumnsSection() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        showExperimentalAppearanceSettings();

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Columns"));
        Assertions.assertTrue(bodyText.contains("Columns count"));
        Assertions.assertTrue(bodyText.contains("Columns horizontal distribution"));
        Assertions.assertTrue(bodyText.contains("Columns vertical distribution"));
        Assertions.assertTrue(bodyText.contains("Column padding left"));
        Assertions.assertTrue(bodyText.contains("Column padding top"));
        Assertions.assertTrue(bodyText.contains("Column padding right"));
        Assertions.assertTrue(bodyText.contains("Column padding bottom"));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnGridColSpanIndicator")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnGridColSpan")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnsHorizontalAlignmentItem")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnsVerticalAlignmentItem")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnPaddingLeft")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnPaddingRight")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnPaddingTop")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#columnPaddingBottom")).size());

    }
}

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

@Disabled
public class FooTest {
    private static final String EXTENSION_ID = "neobmmphlmmomdccnocemcffefagdabf";
    private static final String DOCKER_EXTENSION_ID = "ebfcaabbdccjnlambmcjngkjhljhhbge";
    private static final String OPTIONS = "chrome-extension://" + DOCKER_EXTENSION_ID + "/options.html";

    @Test
    public void foo() throws IOException, InterruptedException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("load-extension=src/main/chrome");
        options.addArguments("--no-sandbox");
        //        options.addArguments("enable-automation");
        //        options.addArguments("test-type=browser");
        //        options.addArguments("disable-plugins");
        //        options.addArguments("disable-infobars");
        ChromeDriver chromeDriver = new ChromeDriver(options);

        //        chromeDriver.get("chrome://newtab");
        chromeDriver.get(OPTIONS);
        //        getChromeSystemInfo(chromeDriver);

        String hostName = InetAddress.getLocalHost().getHostName();
        File screenshotFile = chromeDriver.getScreenshotAs(OutputType.FILE);
        String datetime = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        FileUtils.copyFile(screenshotFile, new File(String.format("/usr/screenshots/screen_%s_%s.png", hostName, datetime)));

        //        String screenshotAs = chromeDriver.getScreenshotAs(OutputType.BASE64);
        //        System.out.println("");
        //        System.out.println("screenshotAs = " + screenshotAs);

        chromeDriver.close();
    }

    private void getChromeSystemInfo(ChromeDriver chromeDriver) throws InterruptedException {
        chromeDriver.get("chrome://system");
        Thread.sleep(5000);
        List<WebElement> buttons = chromeDriver.findElementsByTagName("button");
        for (WebElement button : buttons) {
            System.out.println("button.getAttribute(\"id\") = " + button.getAttribute("id"));
        }

        chromeDriver.findElementById("expandAll").click();
        Thread.sleep(5000);
    }
}


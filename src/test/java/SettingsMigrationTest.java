import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SettingsMigrationTest extends ExtensionLoader {

    @Test
    void migrationFrom1_0To1_1() {
        openOptions();
        String remove1_1DefaultSettingsScript = """
            chrome.storage.local.clear();
            """;
        driver.executeScript(remove1_1DefaultSettingsScript);

        String set1_0SettingsScript = """
            chrome.storage.local.set({
                theme: 'cosmo',
                keyboardSupport: true,
                environment: 0,
                content: {
                    0: 'home',
                    1: 'work'
                 }
            });""";
        driver.executeScript(set1_0SettingsScript);

        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-content");
        Assertions.assertEquals(2, driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr").size());

        WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
        selectEnvironmentInTheTable(Environment.home.getName());
        Assertions.assertEquals("home", textarea.getAttribute("value"));

        selectEnvironmentInTheTable(Environment.work.getName());
        Assertions.assertEquals("work", textarea.getAttribute("value"));

        openEnvironmentDetails("tab-environment-theme");
        Assertions.assertEquals("Applied", driver.findElementByCssSelector("#tab-environment-theme .card[theme=cosmo] button").getText());
        Assertions.assertEquals("Apply theme", driver.findElementByCssSelector("#tab-environment-theme .card[theme=default] button").getText());

        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        Assertions.assertTrue(keyboardSupportCheckbox.isSelected());
    }
}

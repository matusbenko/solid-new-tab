import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AppearanceBackgroundTest extends ExtensionLoader {
    @Test
    void backgroundSettingWorks() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");
        String setOpacityScript = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.environment[1].appearance.background.opacity = 0.2;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(setOpacityScript);
        WebElement item = driver.findElementByCssSelector("#backgroundImageUrlItem");
        item.sendKeys("http://foo.com/bar");
        item.sendKeys(Keys.TAB);

        shortWait.withTimeout(Duration.of(30, ChronoUnit.SECONDS)).until(driver -> {
            Object isBackgroundImageUrlSettingSaved = ((ChromeDriver) driver).executeAsyncScript("""
                var done = arguments[0];
                chrome.storage.local.get('solidNewTabSettings', function(data) {
                    const value = data.solidNewTabSettings.environment[1].appearance.background.imageUrl;
                    done(value !== undefined);
                });
                """);
            System.out.println("isBackgroundImageUrlSettingSaved = " + isBackgroundImageUrlSettingSaved);
            return isBackgroundImageUrlSettingSaved;
        });

        openNewTab();
        WebElement backgroundUnderlay = driver.findElementByCssSelector("#background-underlay");
        Assertions.assertEquals("url(\"http://foo.com/bar\")", backgroundUnderlay.getCssValue("background-image"));
        Assertions.assertEquals("0.2", backgroundUnderlay.getCssValue("opacity"));
    }

    @Test
    void optionsPageAppearanceTabBackgroundSection() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-appearance");

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Background"));
        Assertions.assertTrue(bodyText.contains("Image URL"));
        Assertions.assertTrue(bodyText.contains("Find nice backgrounds for example at https://wallhaven.cc/"));
        Assertions.assertTrue(bodyText.contains("Image opacity"));
        Assertions.assertTrue(bodyText.contains("Transparent"));
        Assertions.assertTrue(bodyText.contains("Opaque"));
        Assertions.assertTrue(bodyText.contains("0.8"));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#backgroundImageUrlItem")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("#backgroundImageOpacityItem")).size());

    }
}

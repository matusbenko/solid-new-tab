import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

class ContentTest extends ExtensionLoader {

    @Test
    void hasColumns() {
        openNewTab();

        for (Environment environment : Environment.all()) {
            setEnvironment(environment);
            List<WebElement> columnList = driver.findElements(By.className("entry-column"));
            Assertions.assertEquals(3, columnList.size());
        }
    }

    @Test
    void hasLinks() {
        Map<Environment, List<String[]>> environmentLinkListMap = new HashMap<>();

        List<String[]> homeColumns = new ArrayList<>();
        homeColumns.add(new String[] { "Google", "Duck Duck Go", "Dictionary", "Translate", "YouTube", "Maps", "Drive", "IP address" });
        homeColumns.add(new String[] { "GitLab", "GitHub", "BitBucket", "Mastodon", "Reddit" });
        homeColumns.add(new String[] { "DW", "RT", "France24", "BBC", "CNN", "CGTN", "Medium", "It's FOSS", "opensource.com" });
        environmentLinkListMap.put(Environment.home, homeColumns);

        ArrayList<String[]> workColumns = new ArrayList<>();
        workColumns.add(new String[] { "O'Reilly Safari books", "Spring projects", "Chrome extension tutorial" });
        workColumns.add(new String[] { "JDK 13", "Spring framework API", "NodeJS API", "Angular API", "React Top-Level API", "Vue.js API",
            "AngularJS API", "jQuery API" });
        workColumns.add(new String[] { "Gradle", "Maven", "Solr", "Elastic Search", "Apache commons", "MySQL Reference", "MongoDB Manual",
            "Oracle DB SQL reference (quick)", "Oracle DB SQL reference" });
        environmentLinkListMap.put(Environment.work, workColumns);

        openNewTab();

        for (Environment environment : Environment.all()) {
            setEnvironment(environment);
            List<WebElement> columnList = driver.findElements(By.className("entry-column"));

            for (int colNum = 0; colNum < columnList.size(); colNum++) {
                WebElement column = columnList.get(colNum);
                List<WebElement> linkList = column.findElements(By.tagName("a"));

                for (int linkNum = 0; linkNum < linkList.size(); linkNum++) {
                    String expected = environmentLinkListMap.get(environment).get(colNum)[linkNum];
                    String actual = linkList.get(linkNum).getText();
                    Assertions.assertEquals(expected, actual);
                }
            }
        }
    }

    @Test
    void hasSectionTitles() {
        Map<Environment, List<String[]>> environmentSectionTitleListMap = new HashMap<>();

        List<String[]> homeColumns = new ArrayList<>();
        homeColumns.add(new String[] { "Tools" });
        homeColumns.add(new String[] { "Programming", "Social" });
        homeColumns.add(new String[] { "News" });
        environmentSectionTitleListMap.put(Environment.home, homeColumns);

        ArrayList<String[]> workColumns = new ArrayList<>();
        workColumns.add(new String[] { "Knowledge" });
        workColumns.add(new String[] { "Docs" });
        workColumns.add(new String[] { "Tools", "Databases" });
        environmentSectionTitleListMap.put(Environment.work, workColumns);

        openNewTab();

        for (Environment environment : Environment.all()) {
            setEnvironment(environment);
            List<WebElement> columnList = driver.findElements(By.className("entry-column"));

            for (int colNum = 0; colNum < columnList.size(); colNum++) {
                WebElement column = columnList.get(colNum);
                List<WebElement> sectionTitleList = column.findElements(By.tagName("h3"));

                for (int sectionTitleNum = 0; sectionTitleNum < sectionTitleList.size(); sectionTitleNum++) {
                    String expected = environmentSectionTitleListMap.get(environment).get(colNum)[sectionTitleNum];
                    String actual = sectionTitleList.get(sectionTitleNum).getText();
                    Assertions.assertEquals(expected, actual);
                }
            }
        }
    }

    @Test
    void hasLineSeparators() {
        Map<Environment, List<Integer>> environmentLineSeparatorCountMap = new HashMap<>();
        environmentLineSeparatorCountMap.put(Environment.home, Arrays.asList(2, 0, 1));
        environmentLineSeparatorCountMap.put(Environment.work, Arrays.asList(0, 2, 2));

        openNewTab();

        for (Environment environment : Environment.all()) {
            setEnvironment(environment);
            List<WebElement> columnList = driver.findElements(By.className("entry-column"));

            for (int colNum = 0; colNum < columnList.size(); colNum++) {
                WebElement column = columnList.get(colNum);
                List<WebElement> lineSeparatorList = column.findElements(By.tagName("hr"));

                int expected = environmentLineSeparatorCountMap.get(environment).get(colNum);
                int actual = lineSeparatorList.size();
                Assertions.assertEquals(expected, actual);
            }
        }
    }

    @Test
    void hasIcons() {
        openNewTab();

        for (Environment environment : Environment.all()) {
            setEnvironment(environment);
            List<WebElement> columnList = driver.findElements(By.className("entry-column"));

            for (WebElement column : columnList) {
                List<WebElement> linkList = column.findElements(By.tagName("i"));
                Assertions.assertTrue(linkList.size() > 0);
            }
        }
    }

    @Test
    void hasDefaultContent() {
        openNewTab();

        List<WebElement> columnList = driver.findElements(By.className("entry-column"));
        Assertions.assertTrue(columnList.size() > 0);

        for (WebElement column : columnList) {
            List<WebElement> linkList = column.findElements(By.tagName("i"));
            Assertions.assertTrue(linkList.size() > 0);
        }
    }

    @Test
    void whenSwitchingEnvironmentsContentIsSwitched() {
        openOptions("tab-environment");
        WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));

        for (Environment environment : Environment.all()) {
            selectEnvironmentInTheTable(environment.getName());
            textarea.clear();
            Assertions.assertEquals("", textarea.getAttribute("value"));
            textarea.sendKeys(environment.name());
            Assertions.assertEquals(environment.name(), textarea.getAttribute("value"));
        }

        for (Environment environment : Environment.all()) {
            selectEnvironmentInTheTable(environment.getName());
            Assertions.assertEquals(environment.name(), textarea.getAttribute("value"));
        }
    }

    @Test
    void oneColumn() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-content");
        WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
        textarea.clear();
        textarea.sendKeys("=Column1");
        openNewTab();
        List<WebElement> columns = driver.findElements(By.className("entry-column"));
        Assertions.assertEquals(1, columns.size());
    }

    @Test
    void tenColumns() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-content");
        WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
        textarea.clear();
        textarea.sendKeys(
            "=Column1\n\n=Column2\n\n=Column3\n\n=Column4\n\n=Column5\n\n=Column6\n\n=Column7\n\n=Column8\n\n=Column9\n\n=Column10");
        openNewTab();
        List<WebElement> columns = driver.findElements(By.className("entry-column"));
        Assertions.assertEquals(10, columns.size());
    }

    @Test
    void optionsPageContentTab() throws InterruptedException {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-content");

        Assertions.assertFalse(driver.findElement(By.cssSelector("table[id='help']")).isDisplayed());
        WebElement helpButton = driver.findElement(By.cssSelector("a[id='help-button']"));
        helpButton.click();
        Thread.sleep(500);
        Assertions.assertTrue(driver.findElement(By.cssSelector("table[id='help']")).isDisplayed());

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Link:"));
        Assertions.assertTrue(bodyText.contains("Widget:"));
        Assertions.assertTrue(bodyText.contains("Section title:"));
        Assertions.assertTrue(bodyText.contains("Line separator:"));
        Assertions.assertTrue(bodyText.contains("Next column:"));
        Assertions.assertTrue(bodyText.contains("Icon:"));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector(OPTION_TEXTAREA_CONTENT)).size());
    }

    @Test
    void syntaxErrorAlert() {
        openOptions("tab-environment");
        openEnvironmentDetails("tab-environment-content");

        WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
        textarea.clear();
        textarea.sendKeys("bad_syntax");

        openNewTab();
        Assertions.assertTrue(driver.findElementByCssSelector("#syntax-error-alert").isDisplayed());
    }
}

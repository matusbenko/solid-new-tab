import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

public class KeyboardSupportTest extends ExtensionLoader {
    @Test
    void ifOptionsPageHasKeyboardSupportSection() {
        openOptions("tab-keyboard");

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("When keyboard support is turned on, opening new tab will move focus from omnibar to the page. The behaviour is similar to Vimium extension."));
        Assertions.assertTrue(bodyText.contains("Open link in the same tab"));
        Assertions.assertTrue(bodyText.contains("Search on google"));
        Assertions.assertTrue(bodyText.contains("Close tab"));
        Assertions.assertTrue(bodyText.contains("Select next tab"));
        Assertions.assertTrue(bodyText.contains("Select previous tab"));
        Assertions.assertTrue(bodyText.contains("Forward in history"));

        Assertions.assertEquals(1, driver.findElements(By.cssSelector("table[id='keyboard-shortcuts']")).size());
        Assertions.assertEquals(1, driver.findElements(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT)).size());
    }
}

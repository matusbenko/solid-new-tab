import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

class ResetTest extends ExtensionLoader {

    private static String defaultContentHome;
    private static String defaultContentWork;

    @BeforeAll
    static void loadDefaultContents() throws IOException {
        defaultContentHome = new String(Files.readAllBytes(Paths.get("src/test/resources/defaultContentHome.txt")));
        defaultContentWork = new String(Files.readAllBytes(Paths.get("src/test/resources/defaultContentWork.txt")));
    }

    @Test
    void ifResetSettingsWorks() {
        changeSettings();

        openOptions("tab-reset");
        WebElement resetSettingsButton = driver.findElement(By.cssSelector("button[name='resetSettings'][type='button']"));
        resetSettingsButton.click();

        openOtherPage();
        verifySettingsResetToDefault();
    }

    @Test
    void ifResetAppearanceSettingsWorks() {
        changeSettings();

        openOptions("tab-reset");
        WebElement resetAppearanceSettingsButton = driver.findElement(By.cssSelector("button[name='resetAppearanceSettings'][type='button']"));
        resetAppearanceSettingsButton.click();

        openOtherPage();
        verifyOnlyAppearanceSettingsResetToDefault();
    }

    private void changeSettings() {
        openOptions("tab-environment");
        driver.findElementByCssSelector("#tab-environment #newEnvironmentButton").click();
        shortWait.until(driver -> driver.findElement(By.cssSelector("#environmentNameField")).isDisplayed());
        driver.findElementByCssSelector("#environmentNameField").sendKeys("Foo");
        driver.findElementByCssSelector("#environmentSaveButton").click();
        shortWait.until(driver -> !driver.findElement(By.cssSelector("#environmentNameField")).isDisplayed());
        //content is already before every test set custom

        for (Environment environment : Environment.all()) {
            selectEnvironmentInTheTable(environment.getName());

            openEnvironmentDetails("tab-environment-content");
            WebElement textarea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
            textarea.clear();
            textarea.sendKeys("=Whatever");

            openEnvironmentDetails("tab-environment-theme");
            driver.findElementByCssSelector("#tab-environment-theme .card[theme=cosmo] button").click();

            openEnvironmentDetails("tab-environment-appearance");
            driver.findElementByCssSelector("#backgroundImageUrlItem").sendKeys("foo");
            driver.findElementByCssSelector("body").click();

            String setOpacityScript = """
                chrome.storage.local.get('solidNewTabSettings', function(data) {
                    data.solidNewTabSettings.environment[%d].appearance.background.opacity = 0.2;
                    chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
                });
                """.formatted(environment.getIndex());
            driver.executeScript(setOpacityScript);

            String setSectionTitleSizeScript = """
                chrome.storage.local.get('solidNewTabSettings', function(data) {
                    data.solidNewTabSettings.environment[%d].appearance.fontSize.sectionTitle=1;
                    chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
                });
                """.formatted(environment.getIndex());
            driver.executeScript(setSectionTitleSizeScript);

            String setLinkSizeScript = """
                chrome.storage.local.get('solidNewTabSettings', function(data) {
                    data.solidNewTabSettings.environment[%d].appearance.fontSize.link=1;
                    chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
                });
                """.formatted(environment.getIndex());
            driver.executeScript(setLinkSizeScript);
        }

        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        keyboardSupportCheckbox.click();
    }

    private void verifyOnlyAppearanceSettingsResetToDefault() {
        openOptions("tab-environment");
        Assertions.assertEquals(3, driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr").size());

        for (Environment environment : Environment.all()) {
            selectEnvironmentInTheTable(environment.getName());

            openEnvironmentDetails("tab-environment-content");
            WebElement contentTextArea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
            Assertions.assertEquals("=Whatever", contentTextArea.getAttribute("value"));

            verifyAppearanceSettingsEqualToDefault();

            openEnvironmentDetails("tab-environment-theme");
            Assertions.assertEquals("Applied", driver.findElementByCssSelector("#tab-environment-theme .card[theme=cosmo] button").getText());
            Assertions.assertEquals("Apply theme", driver.findElementByCssSelector("#tab-environment-theme .card[theme=default] button").getText());
        }

        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        Assertions.assertTrue(keyboardSupportCheckbox.isSelected());
    }

    private void verifySettingsResetToDefault() {
        openOptions("tab-environment");
        Assertions.assertEquals(2, driver.findElementsByCssSelector("#tab-environment #environments-table tbody tr").size());

        for (Environment environment : Environment.all()) {
            selectEnvironmentInTheTable(environment.getName());

            openEnvironmentDetails("tab-environment-content");
            WebElement contentTextArea = driver.findElement(By.cssSelector(OPTION_TEXTAREA_CONTENT));
            Assertions.assertEquals(getDefaultContent(environment), contentTextArea.getAttribute("value"));

            verifyAppearanceSettingsEqualToDefault();

            openEnvironmentDetails("tab-environment-theme");
            Assertions.assertEquals("Apply theme", driver.findElementByCssSelector("#tab-environment-theme .card[theme=cosmo] button").getText());
            Assertions.assertEquals("Applied", driver.findElementByCssSelector("#tab-environment-theme .card[theme=default] button").getText());
        }

        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        Assertions.assertFalse(keyboardSupportCheckbox.isSelected());
    }

    private void verifyAppearanceSettingsEqualToDefault() {
        openEnvironmentDetails("tab-environment-appearance");
        Assertions.assertEquals("", driver.findElementByCssSelector("#backgroundImageUrlItem").getAttribute("value"));
        Assertions.assertEquals("0.8", driver.findElementByCssSelector("#backgroundImageOpacityItem").getAttribute("value"));
        Assertions.assertEquals("3", driver.findElementByCssSelector("#sectionTitleSizeItem").getAttribute("value"));
        Assertions.assertEquals("0.875", driver.findElementByCssSelector("#linkSizeItem").getAttribute("value"));
    }

    private String getDefaultContent(Environment environment) {
        return switch (environment) {
            case home: yield defaultContentHome;
            case work: yield defaultContentWork;
        };
    }

    @Test
    void optionsPageResetTab() {
        openOptions("tab-reset");

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("Reset to default settings. All settings will be lost!"));
        Assertions.assertEquals(1, driver.findElements(By.cssSelector("button[name='resetSettings'][type='button']")).size());
    }
}

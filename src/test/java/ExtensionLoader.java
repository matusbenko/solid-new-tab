import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

class ExtensionLoader {
    public static final String OPTION_CHECKBOX_KEYBOARD_SUPPORT = "input[name='keyboardSupport'][type='checkbox']";
    static final String OPTION_TEXTAREA_CONTENT = "textarea[name='content']";
    static final String NEW_TAB = "chrome://newtab";
    protected WebDriverWait shortWait;
    ChromeDriver driver;
    @RegisterExtension ScreenshotExtension screenshotExtension = new ScreenshotExtension();
    @RegisterExtension PrintOutCurrentTestExtension printOutCurrentTestExtension = new PrintOutCurrentTestExtension();

    @BeforeEach
    public void setup() {
        System.out.println("-- Setup started");
        loadUnpackedExtension();
        setTestContent();
        System.out.println("-- Setup finished");
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    private void loadUnpackedExtension() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("load-extension=src/main/chrome");
        options.addArguments("--no-sandbox");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        shortWait = new WebDriverWait(driver, 5);
    }

    protected void setTestContent() {
        String testContentHome;
        String testContentWork;

        try {
            testContentHome = new String(Files.readAllBytes(Paths.get("src/test/resources/testContentHome.txt")));
            testContentWork = new String(Files.readAllBytes(Paths.get("src/test/resources/testContentWork.txt")));
        } catch (IOException e) {
            throw new RuntimeException();
        }

        openOptions();
        testContentHome = testContentHome.replace("\r\n", "\\n").replace("\n", "\\n").replace("\"", "\\\"");
        testContentWork = testContentWork.replace("\r\n", "\\n").replace("\n", "\\n").replace("\"", "\\\"");

        String script = """
            chrome.storage.local.set({
                solidNewTabSettings: {
                    version: '1.1',
                    keyboardSupport: false,
                    environment: [{
                        id: '0bb2d437-8509-439e-b410-190421b75563',
                        name: 'Home',
                        active: false,
                        icon: 'fas fa-home',
                        content: "%s",
                        theme: 'default',
                        appearance: {
                            background: {
                                imageUrl: undefined,
                                opacity: 0.8
                            },
                            fontSize: {
                                sectionTitle: 3,
                                link: 0.875
                            },
                            columns: {
                                colSpan: 2,
                                padding: {
                                    left: 15,
                                    top: 100,
                                    right: 15,
                                    bottom: 0
                                },
                                verticalAlignment: 'align-items-start',
                                horizontalAlignment: 'justify-content-center'
                            }
                        }
                    },{
                        id: '42e462b0-c684-4bc4-9d0e-7f2327f4ed7b',
                        name: 'Work',
                        active: true,
                        icon: 'fas fa-briefcase',
                        content: "%s",
                        theme: 'default',
                        appearance: {
                            background: {
                                imageUrl: undefined,
                                opacity: 0.8
                            },
                            fontSize: {
                                sectionTitle: 3,
                                link: 0.875
                            },
                            columns: {
                                colSpan: 2,
                                padding: {
                                    left: 15,
                                    top: 100,
                                    right: 15,
                                    bottom: 0
                                },
                                verticalAlignment: 'align-items-start',
                                horizontalAlignment: 'justify-content-center'
                            }
                        }
                    }]
                }
            });""".formatted(testContentHome, testContentWork);
        driver.executeScript(script);
        System.out.println("Test content script executed");
    }

    void openOptions() {
        System.out.println("Open Options");
        openNewTab();
        driver.findElementById("settings").click();
        waitForMagic();
        switchWindowHandle("Solid New Tab options");
        waitForOptionsContent("tab-environment");
    }

    void openEnvironmentDetails(String tab) {
        System.out.printf("Open environment details tab '%s'%n", tab);
        WebElement tabElement = driver.findElement(By.cssSelector(".nav-link[content='" + tab + "']"));
        tabElement.click();
        shortWait.until(driver -> tabElement.getAttribute("class").contains("active"));
    }

    void openOptions(String tab) {
        System.out.printf("Open Options tab '%s'%n", tab);
        openOptions();
        //seems to be common problem that element is found while style is not yet loaded
        shortWait.ignoring(ElementClickInterceptedException.class).until(driver -> {
            WebElement navigationLink = driver.findElement(By.cssSelector(".nav-link[content='" + tab + "']"));
            navigationLink.click();
            return true;
        });
        waitForOptionsContent(tab);
    }

    void openNewTab() {
        System.out.println("Open New Tab");
        driver.get(NEW_TAB);
        waitForMagic();
        switchWindowHandle("New tab");
        waitForLoadedBody();
    }

    private void switchWindowHandle(String windowTitle) {
        Set<String> windowHandles = driver.getWindowHandles();
        for (String windowHandle : windowHandles) {
            boolean isTitleEqual = driver.switchTo().window(windowHandle).getTitle().equals(windowTitle);
            if (isTitleEqual) {
                break;
            }
        }
    }

    private void waitForOptionsContent(String tab) {
        shortWait.until(driver -> {
            WebElement navigationLink2 = driver.findElement(By.cssSelector(".nav-link[content='" + tab + "']"));
            boolean isActive = navigationLink2.getAttribute("class").contains("active");
            System.out.printf("'%s' active = %s%n", tab, isActive);
            return isActive;
        });
    }

    // Explicit wait which serves to avoid errors of intercepted clicks while late style load kicks in and repositions elements.
    // Also need when keyboard support is turned on and the new tab does the redirect.
    private void waitForMagic() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
    }

    private void waitForLoadedBody() {
        shortWait.until(driver -> {
            WebElement body = driver.findElement(By.tagName("body"));
            String bodyCssClass = body.getAttribute("class");
            boolean isLoaded = bodyCssClass.contains("loaded");
            System.out.println("Is body loaded = " + isLoaded);
            return isLoaded;
        });
    }

    void setEnvironment(Environment environment) {
        driver.findElement(By.cssSelector("a[id='" + environment.getId() + "']")).click();
    }

    protected void enableKeyboardSupportAndSwitchToNewTab() {
        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        keyboardSupportCheckbox.click();
        openNewTab();
    }
    protected void openOtherPage() {
        driver.get("https://google.com");
    }

    protected void selectEnvironmentInTheTable(String environmentName) {
        driver.findElement(By.xpath("//div[@id='tab-environment']//tbody//td[text()='" + environmentName + "']/..//input[@type='radio']"))
            .click();
    }

    protected void showExperimentalAppearanceSettings() {
        driver.findElement(By.cssSelector("#experimentalToggle")).click();
        shortWait.until(driver -> driver.findElement(By.cssSelector("#tab-environment-appearance-experimental")).getAttribute("class")
            .contains("show"));
    }

    protected enum Environment {
        home(0, "0bb2d437-8509-439e-b410-190421b75563", "Home"),
        work(1, "42e462b0-c684-4bc4-9d0e-7f2327f4ed7b", "Work");

        private int index;
        private String id;
        private String name;

        Environment(int index, String id, String name) {
            this.index = index;
            this.id = id;
            this.name = name;
        }

        // After added IDs to enum values() doesn't work!?
        public static Environment[] all() {
            return new Environment[] { home, work };
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getIndex() {
            return index;
        }
    }
}

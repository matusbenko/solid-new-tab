import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class KeyboardSupportNormalModeTest extends ExtensionLoader {
    // testIfCanSelectNext/PreviousTab not implemented because selenium can't find active tab in browser

    @Test
    void ifCanCloseTab() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("x");
        WebDriverWait waitForClose = new WebDriverWait(driver, 5);
        waitForClose.until(webDriver -> {
            try {
                driver.getTitle();
            } catch (NoSuchWindowException e) {
                return true;
            }
            return false;
        });
    }

    @Test
    void ifCanGoForwardInHistory() {
        enableKeyboardSupportAndSwitchToNewTab();
        driver.get("https://google.com");
        Assertions.assertEquals("Google", driver.getTitle());

        driver.navigate().back();
        Assertions.assertEquals("New tab", driver.getTitle());

        WebElement document = driver.findElement(By.cssSelector("html"));
        String forwardShortcut = Keys.chord(Keys.SHIFT, "l");
        document.sendKeys(forwardShortcut);
        Assertions.assertEquals("Google", driver.getTitle());
    }

    @Test
    void ifKeyboardSupportOptionIsPersisted() {
        openOptions("tab-keyboard");
        WebElement keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        Assertions.assertFalse(keyboardSupportCheckbox.isSelected());

        keyboardSupportCheckbox.click();
        Assertions.assertTrue(keyboardSupportCheckbox.isSelected());

        driver.get("http://google.com");
        openOptions("tab-keyboard");
        keyboardSupportCheckbox = driver.findElement(By.cssSelector(OPTION_CHECKBOX_KEYBOARD_SUPPORT));
        Assertions.assertTrue(keyboardSupportCheckbox.isSelected());
    }
}

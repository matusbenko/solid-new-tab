import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NotificationTest extends ExtensionLoader {
    @Test
    void notificationAfterUpdate() {
        String script = """
            chrome.storage.local.get('solidNewTabSettings', function(data) {
                data.solidNewTabSettings.lastOpenNotification = 10000;
                chrome.storage.local.set({ solidNewTabSettings: data.solidNewTabSettings });
            });
            """;
        driver.executeScript(script);
        // after first open of updated version, icon bounces
        openNewTab();
        WebElement notificationIcon = driver.findElement(By.cssSelector("#notification i"));
        Assertions.assertTrue(notificationIcon.getAttribute("class").contains("bounce"));

        notificationIcon.click();
        WebElement notificationWindow = driver.findElement(By.cssSelector("#notification-window"));
        Assertions.assertTrue(notificationWindow.isDisplayed());
        notificationWindow.findElement(By.cssSelector(".close")).click();
        Assertions.assertFalse(notificationWindow.isDisplayed());

        // after modal is opened, icon doesn't bounce anymore
        Assertions.assertFalse(notificationIcon.getAttribute("class").contains("bounce"));

        // after reload, bounce is left without bounce
        openNewTab();
        Assertions.assertFalse(notificationIcon.getAttribute("class").contains("bounce"));
    }

    @Test
    void notificationForNewUsers() {
        openNewTab();
        WebElement notificationIcon = driver.findElement(By.cssSelector("#notification i"));
        Assertions.assertFalse(notificationIcon.getAttribute("class").contains("bounce"));
    }
}


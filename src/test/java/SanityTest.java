import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SanityTest extends ExtensionLoader {

    @Test
    void verifyNewTabPage() {
        openNewTab();
        String title = driver.getTitle();
        Assertions.assertEquals("New tab", title);
    }

    @Test
    void verifyOptionPage() {
        openOptions();
        String title = driver.getTitle();
        Assertions.assertEquals("Solid New Tab options", title);
    }

}

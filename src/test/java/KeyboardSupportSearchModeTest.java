import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

class KeyboardSupportSearchModeTest extends ExtensionLoader {
    @Test
    void ifSearchWindowIsOpened() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        Assertions.assertTrue(driver.findElement(By.cssSelector("#search-modal-window")).isDisplayed());
        Assertions.assertTrue(driver.findElement(By.cssSelector("#search-input")).isDisplayed());
        Assertions.assertTrue(driver.findElement(By.cssSelector(".modal-backdrop")).isDisplayed());
    }

    @Test
    void ifSearchIsExecuted() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        WebElement searchInput = driver.findElement(By.cssSelector("#search-input"));
        searchInput.sendKeys("answer to life the universe and everything");
        searchInput.sendKeys(Keys.ENTER);

        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assertions.assertTrue(bodyText.contains("42"));
        Assertions.assertTrue(driver.getCurrentUrl().contains("google.com"));
    }

    @Test
    void ifEscapeClosesSearchWindowAndReturnsToNormalMode() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        WebElement searchInput = driver.findElement(By.cssSelector("#search-input"));
        searchInput.sendKeys(Keys.ESCAPE);
        Assertions.assertFalse(driver.findElement(By.cssSelector("#search-modal-window")).isDisplayed());

        document.sendKeys("s");
        Assertions.assertTrue(driver.findElement(By.cssSelector("#search-modal-window")).isDisplayed());
    }

    @Test
    void ifClickOnModalBackdropClosesSearchWindowAndReturnsToNormalMode() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        Actions clickModalBackdropAction = new Actions(driver);
        clickModalBackdropAction.moveByOffset(10, 10);
        clickModalBackdropAction.click();
        clickModalBackdropAction.perform();

        Assertions.assertFalse(driver.findElement(By.cssSelector("#search-modal-window")).isDisplayed());

        document.sendKeys("s");
        Assertions.assertTrue(driver.findElement(By.cssSelector("#search-modal-window")).isDisplayed());
    }

    @Test
    void ifSearchInputIsClearedAfterSearchWindowIsClosed() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        WebElement searchInput = driver.findElement(By.cssSelector("#search-input"));
        searchInput.sendKeys("answer to life the universe and everything");
        searchInput.sendKeys(Keys.ESCAPE);

        document.sendKeys("s");
        Assertions.assertEquals("", searchInput.getText());
    }

    @Test
    void ifSearchInputIsClearedWhenReturnedBackToNewTabFromGoogle() {
        enableKeyboardSupportAndSwitchToNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));

        document.sendKeys("s");
        WebElement searchInput = driver.findElement(By.cssSelector("#search-input"));
        searchInput.sendKeys("answer to life the universe and everything");
        searchInput.sendKeys(Keys.ENTER);

        driver.executeScript("window.history.go(-1)");

        WebElement document2 = driver.findElement(By.cssSelector("html"));
        document2.sendKeys("s");
        WebElement searchInput2 = driver.findElement(By.cssSelector("#search-input"));
        Assertions.assertEquals("", searchInput2.getText());
    }
}

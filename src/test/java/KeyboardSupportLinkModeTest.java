import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

class KeyboardSupportLinkModeTest extends ExtensionLoader {

    @Test
    void ifEnabledKeyboardSupportWorks() {
        enableKeyboardSupportAndSwitchToNewTab();

        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("f");
        Assertions.assertEquals(20, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
        Assertions.assertEquals(2, driver.findElements(By.cssSelector("div[class='navigation-tooltip navigation-tooltip-environment']")).size());
        Assertions.assertEquals(22, driver.findElements(By.cssSelector("span[class=navigation-tooltip-inner]")).size());
        Assertions.assertEquals(44, driver.findElements(By.cssSelector("span[class=navigation-tooltip-inner-id-letter]")).size());
    }

    @Test
    void ifDisabledKeyboardSupportWorks() {
        // Keyboard support disabled by default

        openNewTab();
        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("f");
        Assertions.assertEquals(0, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
    }

    @Test
    void ifTypingLettersFiltersTheNavigationTooltips() {
        enableKeyboardSupportAndSwitchToNewTab();

        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("fs");
        Assertions.assertEquals(2, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
    }

    @Test
    void selectOneLink() {
        enableKeyboardSupportAndSwitchToNewTab();

        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("fsj");
        Assertions.assertEquals("http://commons.apache.org/", driver.getCurrentUrl());
    }

    @Test
    void switchingEnvironmentWillSetNormalMode() {
        enableKeyboardSupportAndSwitchToNewTab();

        setEnvironment(Environment.home);
        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("f");
        setEnvironment(Environment.work);

        Assertions.assertEquals(0, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
        document.sendKeys("f");
        Assertions.assertNotEquals(0, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
    }

    @Test
    void hittingEscapeWillSwitchBackToNormalMode() {
        enableKeyboardSupportAndSwitchToNewTab();

        WebElement document = driver.findElement(By.cssSelector("html"));
        document.sendKeys("f");
        Assertions.assertNotEquals(0, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
        document.sendKeys(Keys.ESCAPE);
        Assertions.assertEquals(0, driver.findElements(By.cssSelector("div[class=navigation-tooltip]")).size());
    }
}
